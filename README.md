# apex-junit

## Usage

Install dependencies

```
$ npm install
```


Run Apex tests

```
$ node index.js --username=<username> --password=<password> --client_id=<client_id> \
    --client_secret=<client_secret> --class=<apex-class-name> --output_dir=<path-to-results>
```

This demo isn't feature complete but meant to serve as an example of triggering Apex unit tests via the Tooling API
and recording the results as Junit for use with CI systems such as Jenkins or Bamboo.

Since the underlying packages (`nforce` & `nforce-tooling`) utilize OAuth for authenticating to you Salesforce
org, you will need a Client ID and Client Secret on top of your Username and Password (password & security token, put together).


