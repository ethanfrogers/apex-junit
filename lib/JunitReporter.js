"use strict";

class JunitReporter {

    constructor(apexTestResult){
        this.testResult = apexTestResult;
        this.xmlResult = null;
    }


    report(){

        const successes = this.testResult.records.filter(record => record.Outcome === "Pass");
        const failures = this.testResult.records.filter(record => record.Outcome !== "Pass");

        const numTestsRun = this.testResult.records.length;
        const numFailures = failures.length;

        let report = `<?xml version="1.0" encoding="UTF-8"?><testsuites><testsuite name="apex" errors="0" failures="${numFailures}" tests="${numTestsRun}">`;

        if(successes){
            successes.forEach( testRun => {
                report += `<testcase classname="${testRun.ApexClass.Name}" name="${testRun.MethodName}" assertions="1"></testcase>`
            });
        }

        if(failures){
            failures.forEach( testRun => {
                report += `<testcase classname="${testRun.ApexClass.Name}" name="${testRun.MethodName}" assertions="1"><failure type="failed" message="${escape(testRun.Message)}">${testRun.StackTrace ? escape(testRun.StackTrace) : ""}</failure></testcase>`;
            });
        }

        report += '</testsuite></testsuites>';

        return report;

    }

}

module.exports = JunitReporter;