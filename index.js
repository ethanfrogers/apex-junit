"use strict";

const nforce = require("nforce");
const Promise = require("bluebird");
const Events = require("events");
const emitter = new Events.EventEmitter();
const PrettyData = require("pretty-data").pd;
const Reporter = require("./lib/junit-reporter");
const Fs = require("fs");
require("nforce-tooling")(nforce);

const args = require("get-them-args")(process.argv.slice(2));

let org = null;

emitter.on("start", (classIds) => {
    console.log("Starting test runs...");
    org.tooling.runTestsAsync({ids: classIds}, (err, jobId) => {
        console.log(`Job Id: ${jobId}`);
        emitter.emit("status", jobId);
    });
});

emitter.on("status", (jobId) => {
     (function getStatus(jobId){
        console.log("Checking test run status...");
        return org.tooling.getAsyncTestStatus({id: jobId}, (err, response) => {
                const completed = response.records.filter(record => record.Status === "Completed");

                if(completed.length === response.records.length){
                    const resultIds = response.records.map(record => record.Id);
                    const uniqueIds = resultIds.filter((id, pos) => resultIds.indexOf(id) == pos );
                    emitter.emit("results", uniqueIds);
                }
                else{
                    setTimeout(() => getStatus(jobId), 10000);
                }
            });
     })(jobId);
});


emitter.on("results", (ids) => {
    console.log("Tests done. Getting results...");
    org.tooling.getAsyncTestResults({ids: ids}, (err, response) => {
        const report = new Reporter(response).report()
        const pretty = PrettyData.xml(report);

        if(!args.output_dir){
            console.log(pretty);
            return emitter.emit("complete");
        }
        else{
            fs.writeFile(args.output_dir, pretty, "utf8", () => {
                emitter.emit("complete");    
            });
        }
    });
});

emitter.on("complete", () => {
    process.exit(0);
});


if(!args.username && !args.password){
    console.error("Username and password required.");
    process.exit(1);
}

if(!args.class){
    console.error("Apex Class name required.");
    process.exit(1);
}


const options = {
    clientId: args.client_id,
    clientSecret: args.client_secret,
    redirectUri: "http://localhost:3000/auth/_callback",
    mode: "single",
    plugins: ["tooling"],
    apiVersion: "v36.0",
    metaOpts: { pollInterval: 10000 }
};

const credentials = {
    username: args.username,
    password: args.password
};


org = nforce.createConnection(options);

org.authenticate(credentials)
    .then(() => {
        return org.tooling.query({
            q: `SELECT Id, Name FROM ApexClass WHERE Name LIKE '${args.class}'`
        });
    })
    .then(results => {
        if(!results.totalSize > 0){
            throw new Error("No records found.");
        }
        
        return Promise.resolve(results.records.map( record => {
            return record.Id;
        }));
    })
    .then(classIds => {
        console.log("Running Apex tests...");
        emitter.emit("start", classIds);
    })
    .catch(err => {
        console.log(err);
        process.exit(1);
    });